# eux.free.fr

On a eu l'idée de découvrir des sites en `.free.fr`, donc on a fait un script vite fait pour découvrir des sites à partir de liste de mots (des prénoms, des mots français, anglais, ...). 

Le script tourne lentement, car l'éxécution des requêtes est séquentielle. Une version asynchrone permettrait de palier à ce problème. Cependant, le language pour créer ce script asycnhrone n'a pas encore été choisi. Peut-être python, node, qui sait.

Le résultat du script est sur notre [site](https://bretzel.ga/free/).

## Listes

*nat2018.csv* --> fichier de base de l'INSEE
 
*prenoms.txt* --> fichier formaté avec `tail -n+2 nat2018.csv | cut -d ";" -f2 | awk '{print tolower($0)}' | iconv -f utf8 -t ascii//TRANSLIT | sort | uniq >> prenoms`

*mots_fr* --> contruit à partir d'une liste trouvée [ici](http://infolingu.univ-mlv.fr/DonneesLinguistiques/Dictionnaires/telechargement.html). La deuxième liste (wml et utf-8) a été utilisée. 
Le processus de formattage utilisé étant un peu complexe, il ne sera pas détaillé ici, afin que si vous souhatiez le faire, vous le fassiez mieux. 

## Script

Utilisation : `./eux.sh <liste_prenoms>`

Le script avec la liste *prenoms.txt* prends un certain temps, le miux est de le laisser tourner tout seul.

La liste des sites trouvés est dans `eux.html`, formatée pour être intégrée à une page web.

## Remerciements

Nicolas Martin pour ses versions pythons du script, même si elles ne sont pas fonctionnelles au moment de l'écriture de ces lignes.

## Analyse des temps

![graphe](./analyse.png)

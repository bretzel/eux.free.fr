#!/bin/bash

if [ $# -lt 2 ] ; then
	echo "Utilisation : $0 <base> <domaine>"
	exit 1
fi

liste_noms="$1"
domaine="$2"
nb_noms=$(cat $liste_noms | wc -l )
nom_dossier_princ=$(basename $2)
nom_dossier=$(basename $1)
nb_invalides=0
nb_valides=0
lettre=$(head -c 1 $liste_noms)
sortie="$nom_dossier_princ/$nom_dossier/$lettre.html"

[ -d $nom_dossier_princ/$nom_dossier ] && rm -rf $nom_dossier_princ/$nom_dossier  
mkdir -p $nom_dossier_princ/$nom_dossier

echo "On en est au $lettre..."

for mot in $(cat $liste_noms) ; do
	url=$mot.$domaine
	ancienne_lettre=$lettre
	lettre=$(echo $mot | head -c 1)
	if [ "$lettre" != "$ancienne_lettre" ]; then
		echo "On en est au $lettre..."
		sortie="$nom_dossier_princ/$nom_dossier/$lettre.html"
	fi

	retour=$(curl -Iw "%{http_code}" $url 2>/dev/null | tail -n 1)
	if [ $retour -eq 200 ] ; then
		echo "Site valide trouvé : http://$url"
		echo "<a href=http://$url target=_blank>$mot</a>" >> $sortie
		echo "<br>" >> $sortie
		((nb_valides++))
	else
		((nb_invalides++))
	fi
done

echo "$nb_invalides" sites invalide\(s\) , "$nb_valides" site valide\(s\) \(sur "$nb_noms" essais\)

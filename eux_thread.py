#!/usr/bin/env python3

import requests
import sys
import os
import shutil
from threading import Thread

sites = []
sortie = ""
lettre = ""

def requete(mot, domaine):
    try:
        if requests.get(f'http://{mot.strip()}.{domaine}').status_code == 200:
            print(f'Site vailde trouvé : {mot.strip()}.{domaine}')
            sites.append(mot)
    except Exception:
        pass

def gen_html(domaine):
    global lettre
    f = open(sortie + lettre + ".html", "w")
    for mot in sites:
        ancienne_lettre = lettre
        lettre = mot[0]
        if ancienne_lettre != lettre:
            f.close()
            f = open(sortie + lettre + ".html", "w")
        f.write(f'<a href="http://{mot.strip()}.{domaine}">{mot.strip()}</a><br>\n')

def main():
    if len(sys.argv) < 3:
        print(f"Usage : {sys.argv[0]} <base> <domaine>")
        sys.exit(1)

    base = open(sys.argv[1], 'r').readlines()
    nom_base = os.path.basename(sys.argv[1])
    domaine = sys.argv[2]
    liste_thread = []

    global sites, sortie, lettre
    sortie = domaine + "/" + nom_base + "/"

    if os.path.isdir(sortie):
        shutil.rmtree(sortie)
    os.makedirs(sortie)

    for mot in base:
        t = Thread(target = requete, args = (mot, domaine))
        liste_thread.append(t)
    
    for t in liste_thread:
        t.start()

    for t in liste_thread:
        t.join()

    sites = sorted(sites)
    lettre = sites[0][0]

    gen_html(domaine)

main()
    


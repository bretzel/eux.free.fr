#!/usr/bin/python3

import grequests
import sys
sys.argv=['', 'prenoms.txt', 'free.fr', 'eux.html']
if len(sys.argv)<4:
    print(f"usage: {sys.argv[0]} fichierListePrenom domain fichierSauvegardeHtml")
    sys.exit(1)


def analyze(liste, domaine):
    total=len(liste)
    listePresents = []
    lettre=''
    async_list=[]
    for ind,prenom in enumerate(liste):
        prenom=prenom.strip()
        print(f'analyze {ind}/{total}')
        if lettre != prenom[0]:
            lettre = prenom[0]
            print(f'on a change de lettre: {lettre}')
        action_item = grequests.get(f'http://{prenom}.{domaine}')
        async_list.append(action_item)
    retour = grequests.map(async_list)
    print(retour)
    for indice, i in enumerate(retour):
        if 200 == i.status_code:
            listePresents.append(liste[indice])
    return listePresents

listePresents =[]
def traitement(response, verify, timeout=1, **kwargs):
    global listePresents
    print(response.text)
    if response.status_code == 200:
        listePresents.append(f'<a href="http://{prenom}.{domaine} target=_blank>{prenom}</a><br>\n')



def sauve(liste, fichier):
    with open(fichier, 'w') as f:
        f.write('<html>\n<body>\n')
        for prenom in liste:
            f.write(prenom)
        f.write('</body>\n</html>')

listePrenom = open(sys.argv[1], 'r').readlines()
# print(f'{listePrenomBrute[1]}---')
# listePrenom = [p.strip() for p in listePrenomBrute]

res = analyze(listePrenom, sys.argv[2])

sauve(res, sys.argv[3])

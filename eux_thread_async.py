#!/usr/bin/env python3

import requests
import sys
import os
import shutil
import asyncio
from threading import Thread

sites = []
sortie = ""
lettre = ""



def traitement(base, index_debut, index_fin, domaine):
    tasks = [requete(base[i], domaine) for i in range(index_debut, index_fin + 1)]
    asyncio.set_event_loop(asyncio.new_event_loop())
    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.gather(*tasks))


async def requete(mot, domaine):
    global sites
    try:
        res = await requests.get(f'http://{mot.strip()}.{domaine}').status_code 
        print(res)
        if res == 200:
            print(f'Site vailde trouvé : {mot.strip()}.{domaine}')
            sites.append(mot)
    except Exception:
        pass

def gen_html(domaine):
    global lettre
    f = open(sortie + lettre + ".html", "w")
    for mot in sites:
        ancienne_lettre = lettre
        lettre = mot[0]
        if ancienne_lettre != lettre:
            f.close()
            f = open(sortie + lettre + ".html", "w")
        f.write(f'<a href="http://{mot.strip()}.{domaine}">{mot.strip()}</a><br>\n')

def main():
    if len(sys.argv) < 3:
        print(f"Usage : {sys.argv[0]} <base> <domaine>")
        sys.exit(1)

    base = open(sys.argv[1], 'r').readlines()
    nom_base = os.path.basename(sys.argv[1])
    domaine = sys.argv[2]
    liste_thread = []

    global sites, sortie, lettre
    sortie = domaine + "/" + nom_base + "/"

    if os.path.isdir(sortie):
        shutil.rmtree(sortie)
    os.makedirs(sortie)

    l = base[0][0]
    index_debut = 0
    index_fin = None

    for i in range(0, len(base)):
        al = l
        l = base[i][0]
        if l != al:
            index_fin = i - 1
            t = Thread(target = traitement, args = (base, index_debut, index_fin,domaine))
            liste_thread.append(t)
            index_debut = i

    tf = Thread(target = traitement, args = (base, index_debut, i,domaine))
    liste_thread.append(tf)

    for t in liste_thread:
        t.start()

    for t in liste_thread:
        t.join()

    sites = sorted(sites)
    print(sites)
    lettre = sites[0][0]

    gen_html(domaine)

main()
    

